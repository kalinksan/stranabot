package main

import (
	"io/ioutil"
	"fmt"
	"os"
	"encoding/json"
	"strings"
)

type countryRu map[string]string
var newCountry countrys

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func getRuFile() countryRu {
	file, e := ioutil.ReadFile("./data/country_ru.json")
	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}
	var countrys countryRu
	json.Unmarshal(file, &countrys)

	return countrys
}

func createRu() {
	countrysRuData := getRuFile()
	for _, countryItem := range countrysData {
		for code, ruName := range countrysRuData {
			if strings.ToLower(countryItem.Code) == strings.ToLower(code) {
				countryItem.NameRu = ruName
				newCountry = append(newCountry, countryItem)
			}
		}
	}

	newCountryJson, _ := json.Marshal(newCountry)
	ioutil.WriteFile("./data/flags.json", newCountryJson, 0644)
}
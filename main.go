package main

import (
	"log"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"io/ioutil"
	"fmt"
	"os"
	"encoding/json"
	"strings"
)
type country struct {
	Code    string `json:"code"`
	Emoji   string `json:"emoji"`
	Unicode string `json:"unicode"`
	Name    string `json:"name"`
	NameRu  string `json:"nameru"`
	Title   string `json:"title"`
}

var numericKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("1"),
		tgbotapi.NewKeyboardButton("2"),
		tgbotapi.NewKeyboardButton("3"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("4"),
		tgbotapi.NewKeyboardButton("5"),
		tgbotapi.NewKeyboardButton("6"),
	),
)

type countrys []country

var countrysData countrys

func init() {
	countrysData = getFlagsFile()
}

func getFlagsFile() countrys {
	file, e := ioutil.ReadFile("./data/flags.json")
	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}
	var countrys countrys
	json.Unmarshal(file, &countrys)

	return countrys
}

func findCountryData(countryName string) (c country, e error) {
	for _, countryItem := range countrysData {
		if strings.ToLower(countryItem.Name) == strings.ToLower(countryName){
			return countryItem, nil
		}

		if strings.ToLower(countryItem.NameRu) == strings.ToLower(countryName){
			return countryItem, nil
		}

		if strings.ToLower(countryItem.Code) == strings.ToLower(countryName){
			return countryItem, nil
		}
	}

	return c, fmt.Errorf("\"%s\"? Не могу найти такое...", countryName)
}

func main() {
	bot, err := tgbotapi.NewBotAPI("562129722:AAGTRcTvoRchrg-SljJkfk75vx18KViuWJo")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		reply := ""

		updMsg := update.Message

		if update.CallbackQuery != nil {
			sendStranaInfo(bot, update.CallbackQuery.Message, update.CallbackQuery.Data)
		}

		if updMsg == nil {
			continue
		}

		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case "start":
				reply = "Привет! Я телеграм-бот.\n\nВведи название страны на русском или английском языке и я расскажу о ней подробнее."
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, reply)
				bot.Send(msg)
			case "select":
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Выбери страну")
				keyboard := tgbotapi.InlineKeyboardMarkup{}
				for _, countryItem := range countrysData {
					var row []tgbotapi.InlineKeyboardButton
					btn := tgbotapi.NewInlineKeyboardButtonData(countryItem.Name, countryItem.Name)
					row = append(row, btn)
					keyboard.InlineKeyboard = append(keyboard.InlineKeyboard, row)
				}
				msg.ReplyMarkup = keyboard
				bot.Send(msg)

			case "select_country":
				i := 0
				var row []tgbotapi.KeyboardButton
				var rows [][]tgbotapi.KeyboardButton
				keyboard := tgbotapi.ReplyKeyboardMarkup{}
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Выбери страну")

				for _, countryItem := range countrysData {
					btn := tgbotapi.NewKeyboardButton(countryItem.Name)
					row = append(row, btn)
					i++
					if i > 4 {
						rows = append(rows, row)
						i = 0
						row = []tgbotapi.KeyboardButton{}
					}
				}
				rows = append(rows, row)
				msg.ReplyMarkup = keyboard
				bot.Send(msg)

			case "open":
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
				msg.ReplyMarkup = numericKeyboard
				bot.Send(msg)
			case "close":
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
				msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
				bot.Send(msg)
			}
		} else {
			sendStranaInfo(bot, updMsg, updMsg.Text)
		}
	}
}

func sendStranaInfo(bot *tgbotapi.BotAPI, updMsg *tgbotapi.Message, findText string) {
	contryData, err := findCountryData(findText)
	wiki := "https://ru.wikipedia.org/wiki/" + strings.Replace(contryData.NameRu, " ", "_", -1)
	msgLink := tgbotapi.NewMessage(updMsg.Chat.ID, wiki)
	if err != nil {
		msgNotFound := tgbotapi.NewMessage(updMsg.Chat.ID, err.Error())
		bot.Send(msgNotFound)
	} else {
		msgFlag := tgbotapi.NewMessage(updMsg.Chat.ID, contryData.Emoji)
		bot.Send(msgLink)
		bot.Send(msgFlag)
	}
}

func getUserName(user tgbotapi.User) string {
	if user.UserName == "" {
		return user.FirstName
	}
	return user.UserName
}